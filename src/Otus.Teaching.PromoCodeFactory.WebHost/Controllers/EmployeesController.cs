﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }
        
        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x => 
                new EmployeeShortResponse()
                    {
                        Id = x.Id,
                        Email = x.Email,
                        FullName = x.FullName,
                    }).ToList();

            return employeesModelList;
        }
        
        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = ToEmployeeResponse(employee);

            return employeeModel;
        }

        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <param name="employeeToAdd"></param>
        /// <returns></returns>
        [HttpPost]
        //[Route("Create")]
        public async Task<ActionResult<EmployeeResponse>> CreateEmployeeAsync([FromBody] EmployeeAddUpdate employeeToAdd)
        {
            var employee = new Employee()
            {
                Id = Guid.NewGuid()
            };

            await SetEmployeeProps(employeeToAdd, employee);

            if (await _employeeRepository.AddAsync(employee))
            {
                return CreatedAtAction(
                    nameof(GetEmployeeByIdAsync), 
                    new {id = employee.Id},
                    ToEmployeeResponse(employee));
            }

            return BadRequest();
        }

        /// <summary>
        /// Редактировать сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="updateEmployee"></param>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        //[Route("Update/")]
        public async Task<ActionResult> UpdateByIdAsync(Guid id, [FromBody] EmployeeAddUpdate updateEmployee)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            await SetEmployeeProps(updateEmployee, employee);
            
            if (await _employeeRepository.UpdateAsync(employee))
            {
                return Ok();
            }

            return BadRequest();
        }
        
        /// <summary>
        /// Удалить сотрудника по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
            {
                return NotFound();
            }

            if (await _employeeRepository.DeleteAsync(id))
            {
                return Ok();
            }

            return BadRequest();
        }

        /// <summary>
        /// Формирует список экземпляров ролей на основе списка имён ролей
        /// </summary>
        /// <param name="rolesNames"></param>
        /// <returns></returns>
        private async Task<List<Role>> CreateRolesList(List<string> rolesNames)
        {
            var existingRoles = (await _rolesRepository.GetAllAsync()).ToList();
            var selectedRoles = new List<Role>();
            foreach (var roleName in rolesNames)
            {
                var role = existingRoles.FirstOrDefault(r => r.Name == roleName);
                if (role != null)
                {
                    selectedRoles.Add(role);
                }
            }

            return selectedRoles;
        }
        
        /// <summary>
        /// Метод заполнения свойств сотрудника с проверками на null
        /// </summary>
        /// <param name="employeeAddUpdate"></param>
        /// <param name="employee"></param>
        /// <returns></returns>
        private async Task SetEmployeeProps(EmployeeAddUpdate employeeAddUpdate, Employee employee)
        {
            if(employeeAddUpdate.FirstName != null)
                employee.FirstName = employeeAddUpdate.FirstName;
            if(employeeAddUpdate.LastName != null)
                employee.LastName = employeeAddUpdate.LastName;
            if(employeeAddUpdate.Email != null)
                employee.Email = employeeAddUpdate.Email;
            if(employeeAddUpdate.AppliedPromocodesCount != null)
                employee.AppliedPromocodesCount = (int)employeeAddUpdate.AppliedPromocodesCount;
            if(employeeAddUpdate.Roles != null)
                employee.Roles = await CreateRolesList(employeeAddUpdate.Roles);
        }

        private EmployeeResponse ToEmployeeResponse(Employee employee)
        {
            return new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };
        }
        
    }
}