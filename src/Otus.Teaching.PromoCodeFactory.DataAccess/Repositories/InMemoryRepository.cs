﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected ConcurrentDictionary<Guid, T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = new ConcurrentDictionary<Guid, T>(
                data.Select(item => 
                    new KeyValuePair<Guid, T>(item.Id, item)));
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data.Values.AsEnumerable());
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            if(Data.TryGetValue(id, out var value))
            {
                return Task.FromResult(value);
            }
            return Task.FromResult(null as T);
        }

        public Task<bool> AddAsync(T entity)
        {
            return Task.FromResult(Data.TryAdd(entity.Id, entity));
        }

        public Task<bool> UpdateAsync(T entity)
        {
            if (Data.TryGetValue(entity.Id, out var oldEntity))
            {
                return Task.FromResult(Data.TryUpdate(entity.Id, entity, oldEntity));
            }
            return Task.FromResult(false);
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            return Task.FromResult(Data.TryRemove(id, out _));
        }
    }
}